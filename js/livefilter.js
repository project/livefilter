(function (Drupal) {
  let doLiveFilter = function () {
    let filterText = this.value.toLowerCase();
    let elementsSelector = this.dataset.elementsSelector;
    // @todo Rename dataset property.
    let textXpath =  this.dataset.textXpath || 'text()';
    let minInput = parseInt(this.dataset.minInput, 10);
    let debug = this.dataset.debug || false;

    const textContents = [];
    const matches = [];
    let elements = document.querySelectorAll(elementsSelector);
    Array.from(elements).forEach(function (element) {

      // Get relevant text via xPath.
      let xPathResult = document.evaluate(textXpath, element);

      let textContent;
      if (xPathResult.resultType === xPathResult.STRING_TYPE) {
        textContent = xPathResult.stringValue;
      } else if (xPathResult.resultType === xPathResult.UNORDERED_NODE_ITERATOR_TYPE) {
        // If not already of type text, get textContent.
        const textParts = [];
        let textNode;
        while (textNode = xPathResult.iterateNext()) {
          textParts.push(textNode.textContent);
        }
        textContent = textParts.join(' ');
      } else {
        // We can't handle bool and int values.
        console.error(`Invalid xPath result in live filter: ${xPathResult.resultType}`)
        return;
      }

      textContents.push(textContent)

      // Only filter when minInput chars.
      let match = (filterText.length < minInput) ? true :
        textContent.toLowerCase().indexOf(filterText) >= 0;
      matches.push(match);

      // Write back match to data-livefilter-match.
      element.dataset.livefilterMatch = match ? '1' : '0';
      // todo: Add highlight. https://codepen.io/tniezurawski/pen/wvzyVEE
    });
    if (debug) {
      console.log('LiveFilterDebug', {elementsSelector, textXpath, minInput, elements, textContents, matches});
    }
  };
  Drupal.behaviors.livefilter = {
    attach: function (context, settings) {
      context.querySelectorAll('.livefilter-value').forEach(
        function (element) {
          element.addEventListener('input', doLiveFilter);
        }
      );
    },
  }
})(Drupal)
