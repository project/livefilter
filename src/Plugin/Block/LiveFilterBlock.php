<?php

namespace Drupal\livefilter\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Search form' block.
 *
 * @Block(
 *   id = "livefilter",
 *   admin_label = @Translation("Live Filter"),
 * )
 */
class LiveFilterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'livefilter',
      '#elements_selector' => $this->configuration['elements_selector'],
      '#text_xpath' => $this->configuration['text_xpath'],
      '#placeholder' => $this->configuration['placeholder'],
      '#size' => $this->configuration['size'],
      '#min_input' => $this->configuration['min_input'],
      '#debug' => $this->configuration['debug'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'elements_selector' => '',
      'text_xpath' => '',
      'placeholder' => '',
      'size' => '',
      'min_input' => 1,
      'debug' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['elements_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Elements selector'),
      '#description' => $this->t('The css selector to find the elements to filter.'),
      '#default_value' => $this->configuration['elements_selector'],
      '#required' => TRUE,
    ];

    $form['text_xpath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text XPath'),
      '#description' => $this->t('The optional XPath to find the text to filter. Use e.g. "@title" for title attribute. Defaults to "text()" for text contained in element.'),
      '#default_value' => $this->configuration['text_xpath'],
    ];

    $form['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#description' => $this->t('The placeholder text displayed in the input element.'),
      '#default_value' => $this->configuration['placeholder'],
    ];

    $form['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Size'),
      '#description' => $this->t('The input element size in characters.'),
      '#default_value' => $this->configuration['size'],
    ];

    $form['min_input'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum input length'),
      '#description' => $this->t('The input length needed to start filtering.'),
      '#default_value' => $this->configuration['min_input'],
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#description' => $this->t('Prints debug information in js console.'),
      '#default_value' => $this->configuration['debug'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['elements_selector'] = $form_state->getValue('elements_selector');
    $this->configuration['text_xpath'] = $form_state->getValue('text_xpath');
    $this->configuration['placeholder'] = $form_state->getValue('placeholder');
    $this->configuration['size'] = $form_state->getValue('size');
    $this->configuration['min_input'] = $form_state->getValue('min_input');
    $this->configuration['debug'] = $form_state->getValue('debug');
  }

}
